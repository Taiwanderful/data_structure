from .list_node import ListNode


class SinglyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def _to_linked_list(self, item):
        if not isinstance(item, ListNode):
            item = ListNode(item)

        return item

    def append(self, item):
        item = self._to_linked_list(item)

        if self.head is None:
            self.head = item
        else:
            self.tail.next = item

        self.tail = item

    def push_front(self, item):
        item = self._to_linked_list(item)

        item.next = self.head
        self.head = item

    def delete(self, data):
        head = self.head
        if head is None:
            return

        if head.data == data:
            self.remove_head()
            return

        current = head

        while current is not None and current.next is not None:
            previous = current
            current = current.next

            if current.data == data:
                previous.next = current.next
                break

    def remove_head(self):
        self.head = self.head.next

    def reverse(self):
        head = self.head
        if head is None:
            return

        previous = None
        current = head
        _next = head.next

        while _next is not None:
            current.next = previous
            # b -> None
            previous = current
            # b
            current = _next
            # d
            _next = current.next
            # None
        current.next = previous
        self.head = current


    def print(self):
        current = self.head
        output = []
        while current is not None:
            output.append(current.data)
            current = current.next
        print(', '.join(output))

if __name__ == '__main__':
    sll = SinglyLinkedList()

    sll.append('b')
    sll.print()
    # b

    sll.append('c')
    sll.append('d')
    print(sll.tail.data)
    # d

    sll.print()
    # b, c, d

    sll.push_front('a')
    sll.print()
    # a, b, c, d

    sll.delete('a')
    sll.print()
    # b, c, d

    sll.delete('c')
    sll.print()
    # b, d

    sll.delete('e')
    sll.print()
    # b, d

    sll.reverse()
    sll.print()
    # d, b