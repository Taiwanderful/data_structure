class ListNode:

    def __init__(self, data):
        self.data = data
        self.next = None


if __name__ == '__main__':
    ll1 = ListNode(1)
    ll1.next = ListNode(2)

    print(ll1.data)
    # 1
    print(ll1.next.data)
    # 2
