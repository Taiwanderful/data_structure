from linked_list.singly_linked_list import SinglyLinkedList


class StackLinkedList:
    def __init__(self):
        self.stack = SinglyLinkedList()
        self._top = -1

    def push(self, data):
        self._top += 1
        self.stack.push_front(data)

    def pop(self):
        if self.is_empty():
            print('This stack is empty')
            return
        data = self.stack.head.data
        self.stack.remove_head()
        self._top -= 1
        return data

    def top(self):
        return self.stack.head.data

    def get_size(self):
        return self._top + 1

    def is_empty(self):
        return self._top == -1


if __name__ == '__main__':
    stack = StackLinkedList()
    stack.push('a')
    stack.push('b')
    stack.push('c')
    stack.push('d')
    stack.push('e')
    print(f'the size of stack is {stack.get_size()}')
    print(f'the top of stack is {stack.top()}')
    while not stack.is_empty():
        print(stack.pop())

    stack.pop()
