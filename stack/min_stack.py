from stack.stack_array import StackArray
from stack.stack_linked_list import StackLinkedList


class MinStack:
    data = StackArray()
    min_stack = StackLinkedList()

    def push(self, value: int):
        self.data.push(value)
        if self.min_stack.is_empty() or value < self.min_stack.top():
            self.min_stack.push(value)
        else:
            self.min_stack.push(self.min_stack.top())

    def pop(self):
        self.data.pop()
        self.min_stack.pop()

    def top(self):
        return self.data.top()

    def is_empty(self):
        return self.data.is_empty()

    def get_size(self):
        return self.data.get_size()

    def get_min(self):
        return self.min_stack.top()

if __name__ == '__main__':
    ms = MinStack()

    ms.pop()

    ms.push(5)
    print(ms.top())
    print(ms.get_min())

    ms.push(4)
    print(ms.top())
    print(ms.get_min())

    ms.push(6)
    print(ms.top())
    print(ms.get_min())

    ms.push(3)
    print(ms.top())
    print(ms.get_min())

    ms.pop()
    print(ms.top())
    print(ms.get_min())