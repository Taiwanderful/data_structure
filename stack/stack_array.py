class StackArray:

    def __init__(self, capacity=2):
        self._top = -1
        self.stack = [None] * capacity
        self.capacity = capacity

    def double_capacity(self):
        self.stack.extend([None] * self.capacity)
        self.capacity *= 2

    def push(self, value):
        self._top += 1
        if self._top == self.capacity:
            self.double_capacity()
        self.stack[self._top] = value

    def pop(self):
        if self.is_empty():
            print('The stack is empty')
            return
        value = self.stack[self._top]
        self.stack[self._top] = None
        self._top -= 1
        return value

    def top(self):
        if self.is_empty():
            print('The stack is empty')
            return
        value = self.stack[self._top]
        return value

    def get_size(self):
        return self._top + 1

    def is_empty(self):
        return self._top == -1

if __name__ == '__main__':
    stack = StackArray()
    stack.push('a')
    stack.push('b')
    stack.push('c')
    stack.push('d')
    stack.push('e')
    print(f'the size of stack is {stack.get_size()}')
    print(f'the top of stack is {stack.top()}')
    while not stack.is_empty():
        print(stack.pop())

    stack.pop()